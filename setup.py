from setuptools import setup
from pathlib import Path

path = Path(__file__).resolve().parent
with open(path/'README.md', encoding='utf-8') as f:
	long_description = f.read()

with open(path/'VERSION') as version_file:
	version = version_file.read().strip()


setup(name='fonos_usa',
	version=version,
	description='Generación y validación de teléfonos de USA',
	url='',
	author='David Pineda/Raquel Bracho',
	author_email='dpineda@uchile.cl',
	license='MIT',
	packages=['fonos_usa'],
	keywords = ["phonenumber", "usa"],
	install_requires=["selenium",],
	entry_points={
		'console_scripts':["phone_usa = fonos_usa.script_tlf:exp",]
		},
	include_package_data=True,
	long_description=long_description,
	long_description_content_type='text/markdown',
	zip_safe=False)
